package com.example.serg3z.customviewgroup;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class MyCustomViewGroup extends ViewGroup {

    private int offset;
    private int maxHeight;


    public MyCustomViewGroup(Context context) {
        this(context, null);
    }

    public MyCustomViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyCustomViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyCustomViewGroup, defStyle, 0);
        offset = a.getDimensionPixelSize(R.styleable.MyCustomViewGroup_offset, 0);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int childCount = getChildCount();
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        int rowWidth = 0;
        int rowCount = 0;
        int childState = 0;
        maxHeight = 0;

        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            // игнорим ребенка
            if (child.getVisibility() == GONE) {
                continue;
            }
            // мерим ребенка
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            childState = combineMeasuredStates(childState, child.getMeasuredState());

            // обрабатываем отступы
            MarginLayoutParams layoutParams = (MarginLayoutParams) child.getLayoutParams();
            int childWidth = child.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin;
            int childHeight = child.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
            maxHeight = Math.max(maxHeight, childHeight);

            // проверяем сможем мы вместить текущего ребенка в эту строку или нет если нет пернесем его на след. строку
            if (rowWidth + childWidth + offset > parentWidth) {
                rowWidth = 0;
                rowCount++;
            }
            // добавляем отступы
            if (rowWidth > 0) {
                rowWidth += offset;
            }

            rowWidth += childWidth;
        }
        int parentHeight = getPaddingTop() + getPaddingBottom() + (maxHeight * (rowCount + 1) + offset * rowCount);
        parentHeight = Math.max(parentHeight, getSuggestedMinimumHeight());
        parentWidth = Math.max(parentWidth, getSuggestedMinimumWidth());
        setMeasuredDimension(
                resolveSizeAndState(parentWidth, widthMeasureSpec, childState),
                resolveSizeAndState(parentHeight, heightMeasureSpec, childState)
        );
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int childCount = getChildCount();

        int paddingLeft = getPaddingLeft();
        int maxWidth = right - left - paddingLeft - getPaddingRight();
        int rowHeight = getPaddingTop();
        int rowWidth = 0;
        int childLeft = 0;
        int childTop = 0;
        int childBottom = 0;
        int childRight = 0;

        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            MarginLayoutParams layoutParams = (MarginLayoutParams) child.getLayoutParams();
            int childWidth = child.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin;

            //Проверим вмещается ли ребенок в текущую строку
            if (rowWidth + childWidth + offset > maxWidth) {
                rowWidth = 0;
                rowHeight += maxHeight + offset;
            }
            // так же как и onMeasure добавляем отступы
            if (rowWidth > 0) {
                rowWidth += offset;
            }

            childLeft = paddingLeft + rowWidth + layoutParams.leftMargin;
            childTop = rowHeight + layoutParams.topMargin;
            childBottom = childTop + maxHeight;
            childRight = childLeft + childWidth + layoutParams.rightMargin;

            child.layout(childLeft, childTop, childRight, childBottom);

            rowWidth += childRight - childLeft;
        }
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new MarginLayoutParams(p);
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof MarginLayoutParams;
    }

}
